package com.dmytrop.banking.main;

import com.google.inject.servlet.GuiceFilter;

import com.dmytrop.banking.modules.MainModule;
import com.dmytrop.banking.modules.RestModule;
import com.dmytrop.banking.servlets.ContextListener;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.jboss.resteasy.plugins.guice.GuiceResteasyBootstrapServletContextListener;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

/**
 * Run Jetty server for application. One program arg is required which is port number.
 */
public class ServerRunner {

  private static final int DEFAULT_PORT = 8080;
  private static final String CONTEXT_PATH = "/";
  private static final String DISPATCHER_MAPPING_URL = "/rest/*";
  private static final Logger logger = LoggerFactory
      .getLogger(ServerRunner.class);

  public static void main(String[] args) {
    if (args.length != 1) {
      throw new IllegalArgumentException(
          "Missing required arguments in application params: <port>");
    }
    new ServerRunner().run(getdPort(args));
  }

  private static int getdPort(String[] args) {
    try {
      return Integer.valueOf(args[0]);
    } catch (Exception ex) {
      return DEFAULT_PORT;
    }
  }

  /**
   * Run the server.
   */
  public void run(int port) {
    Server server = new Server(port);
    server.setHandler(createHandler());
    try {
      server.start();
      server.join();
    } catch (Exception e) {
      logger.error(e.getMessage());
    }
  }

  /**
   * Runs the server without blocking current thread to be used in tests.
   */
  public void runForTest(int port) {
    Server server = new Server(port);
    server.setHandler(createHandler());
    try {
      server.start();
    } catch (Exception e) {
      logger.error(e.getMessage());
    }
  }

  private Handler createHandler() {
    ServletContextHandler handler = new ServletContextHandler();
    handler.setContextPath(CONTEXT_PATH);

    handler.setInitParameter("resteasy.guice.modules",
        MainModule.class.getName() + "," + RestModule.class.getName());
    handler.setInitParameter("resteasy.scan", "true");
    handler.setInitParameter("resteasy.servlet.mapping.prefix", "/rest");

    handler.addEventListener(new ContextListener());
    handler
        .addEventListener(new GuiceResteasyBootstrapServletContextListener());
    handler
        .addFilter(new FilterHolder(new GuiceFilter()), "/*", EnumSet.of(DispatcherType.REQUEST));
    handler.addServlet(
        new ServletHolder(new HttpServletDispatcher()),
        DISPATCHER_MAPPING_URL);
    return handler;
  }

}
