package com.dmytrop.banking.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Digits;

@Entity
public class BankAccount extends AbstractEntity {

  @Column(unique = true, nullable = false)
  private String number;
  @Digits(integer = 100, fraction = 2)
  private BigDecimal balance;

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }
}
