package com.dmytrop.banking.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Digits;

/**
 * Entity to track all transfers between accounts.
 */
@Entity
public class Transaction extends AbstractEntity {

  @ManyToOne
  private BankAccount credit;
  @ManyToOne
  private BankAccount debit;
  private Date creationDate;
  @Digits(integer = 100, fraction = 2)
  private BigDecimal amount;

  public BankAccount getCredit() {
    return credit;
  }

  public void setCredit(BankAccount credit) {
    this.credit = credit;
  }

  public BankAccount getDebit() {
    return debit;
  }

  public void setDebit(BankAccount debit) {
    this.debit = debit;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }
}
