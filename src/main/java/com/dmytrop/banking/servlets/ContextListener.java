package com.dmytrop.banking.servlets;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import javax.servlet.ServletContextEvent;

public class ContextListener extends GuiceServletContextListener {

  private final Logger logger = LoggerFactory.getLogger(ContextListener.class);

  @Override
  protected Injector getInjector() {
    return Guice.createInjector();
  }

  @Override
  public void contextInitialized(ServletContextEvent servletContextEvent) {
    try {
      System.getProperties()
          .load(getClass().getResourceAsStream("/config/main.properties"));
    } catch (IOException e) {
      logger.error("Failed: to load properties: 'main.properties'" + e);
      throw new RuntimeException(e);
    }
    super.contextInitialized(servletContextEvent);
  }

}
