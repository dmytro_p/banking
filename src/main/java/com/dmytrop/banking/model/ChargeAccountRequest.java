package com.dmytrop.banking.model;

/**
 * Request entity to charge account with some amount.
 */
public class ChargeAccountRequest {

  private String wallet;
  private double amount;

  protected ChargeAccountRequest() {
  }

  public ChargeAccountRequest(String wallet, double amount) {
    this.wallet = wallet;
    this.amount = amount;
  }

  public String getWallet() {
    return wallet;
  }

  public void setWallet(String wallet) {
    this.wallet = wallet;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }
}
