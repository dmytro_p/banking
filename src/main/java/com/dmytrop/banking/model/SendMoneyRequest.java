package com.dmytrop.banking.model;

/**
 * Request entity to make money transfer.
 */
public class SendMoneyRequest {

  private String fromWallet;
  private String toWallet;
  private double amount;

  public SendMoneyRequest() {
  }

  public SendMoneyRequest(String fromWallet, String toWallet, double amount) {
    this.fromWallet = fromWallet;
    this.toWallet = toWallet;
    this.amount = amount;
  }

  public String getFromWallet() {
    return fromWallet;
  }

  public void setFromWallet(String fromWallet) {
    this.fromWallet = fromWallet;
  }

  public String getToWallet() {
    return toWallet;
  }

  public void setToWallet(String toWallet) {
    this.toWallet = toWallet;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }
}
