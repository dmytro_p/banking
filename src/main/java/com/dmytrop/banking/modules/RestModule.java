package com.dmytrop.banking.modules;

import com.google.inject.Binder;
import com.google.inject.Module;

import com.dmytrop.banking.resources.BankAccountResource;
import com.dmytrop.banking.resources.TransactionResource;

/**
 * Module to bind Rest Resources.
 */
public class RestModule implements Module {

  @Override
  public void configure(Binder binder) {
    binder.bind(BankAccountResource.class);
    binder.bind(TransactionResource.class);
  }
}
