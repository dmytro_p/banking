package com.dmytrop.banking.modules;

import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.ServletModule;

import com.dmytrop.banking.resources.BankAccountResource;
import com.dmytrop.banking.services.BankAccountService;
import com.dmytrop.banking.services.TransactionService;

/**
 * Main guice module.
 */
public class MainModule extends ServletModule {

  @Override
  protected void configureServlets() {
    install(new JpaPersistModule(System.getProperty("jpa.unit")));
    filter("/*").through(PersistFilter.class);
    bind(BankAccountService.class);
    bind(TransactionService.class);
  }

}
