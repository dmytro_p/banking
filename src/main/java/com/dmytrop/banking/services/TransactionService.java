package com.dmytrop.banking.services;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

import com.dmytrop.banking.entities.BankAccount;
import com.dmytrop.banking.entities.Transaction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

/**
 * Service to manage transactions.
 */
@Singleton
public class TransactionService extends DaoService {

  @Inject
  public TransactionService(
      Provider<EntityManager> entityManagerProvider) {
    super(entityManagerProvider);
  }

  @Transactional
  public List<Transaction> findAll() {
    return find("select tx from Transaction tx", ImmutableMap.of(), Transaction.class);
  }

  /**
   * Creates and persist new transaction.
   * @param credit Account which charged
   * @param debit Account where money is get from.
   * @param amount Transaction amount.
   * @return Create transaction entity.
   */
  @Transactional
  public Transaction create(BankAccount credit, BankAccount debit, double amount) {
    Transaction tx = new Transaction();
    tx.setAmount(BigDecimal.valueOf(amount));
    tx.setCredit(credit);
    tx.setDebit(debit);
    tx.setCreationDate(new Date());
    tx = create(tx);
    return tx;
  }
}
