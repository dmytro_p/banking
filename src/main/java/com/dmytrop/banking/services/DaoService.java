package com.dmytrop.banking.services;

import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * Class contains basic CRUD operation for DB.
 */
public abstract class DaoService {

  private static final Logger logger = LoggerFactory.getLogger(DaoService.class);

  private Provider<EntityManager> entityManagerProvider;

  protected DaoService(Provider<EntityManager> entityManagerProvider) {
    this.entityManagerProvider = entityManagerProvider;
  }

  /**
   * Create new entity.
   *
   * @param entity Entity data.
   * @param <T> type of the entity
   * @return Instance of the persisted entity.
   */
  @Transactional
  public <T> T create(T entity) {
    return this.createEntity(entity);
  }

  protected <T> T createEntity(T entity) {
    em().persist(entity);
    entity = em().merge(entity);
    return entity;
  }

  protected <T> T update(T entity) {
    entity = em().merge(entity);
    em().persist(entity);
    return entity;
  }


  protected <T> List<T> find(String jpql, Map<String, Object> params,
      Class<T> clazz) {
    TypedQuery<T> query = em().createQuery(jpql, clazz);
    params.forEach(query::setParameter);
    return query.getResultList();
  }

  protected <T> T findSingleEntity(String jpql, Map<String, Object> params,
      Class<T> clazz) {
    TypedQuery<T> query = em().createQuery(jpql, clazz);
    params.forEach(query::setParameter);
    try {
      return query.getSingleResult();
    } catch (Exception ex) {
      return null;
    }
  }

  private EntityManager em() {
    return this.entityManagerProvider.get();
  }
}
