package com.dmytrop.banking.services;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

import com.dmytrop.banking.entities.BankAccount;
import com.dmytrop.banking.entities.Transaction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

/**
 * Service to manage bank accounts.
 */
@Singleton
public class BankAccountService extends DaoService {

  private final TransactionService transactionService;

  @Inject
  public BankAccountService(Provider<EntityManager> entityManagerProvider,
      TransactionService transactionService) {
    super(entityManagerProvider);
    this.transactionService = transactionService;
  }

  /**
   * Transfers money from one wallet to another using their numbers.
   *
   * @param fromWallet Wallet number where money come out.
   * @param toWallet Wallet number where money come in.
   * @param amount Money amount
   */
  @Transactional
  public boolean sendMoney(String fromWallet, String toWallet, double amount) {
    List<BankAccount> accounts = find("select ba from BankAccount ba "
            + "where ba.number = :from or ba.number = :to",
        ImmutableMap.of("from", fromWallet, "to", toWallet), BankAccount.class);
    if (accounts.size() != 2) {
      throw new IllegalArgumentException("One of the wallets does not exist");
    }
    int fromIndex = accounts.get(0).getNumber().equals(fromWallet) ? 0 : 1;
    BankAccount from = accounts.get(fromIndex);
    BankAccount to = accounts.get(fromIndex == 0 ? 1 : 0);

    Transaction tx = transactionService.create(to, from, amount);

    BigDecimal transferAmount = BigDecimal.valueOf(amount);
    if (transferAmount.compareTo(from.getBalance()) > 0) {
      return false;
    }
    BigDecimal fromBalance = from.getBalance().subtract(transferAmount);
    BigDecimal toBalance = to.getBalance().add(transferAmount);
    from.setBalance(fromBalance);
    to.setBalance(toBalance);

    create(tx);
    update(from);
    update(to);

    return true;
  }

  /**
   * Put amount on bank account balance.
   *
   * @param bankAccount Account where money should be put.
   * @param amount Amount to be put.
   * @return Updated bank account.
   */
  @Transactional
  public BankAccount chargeAccount(BankAccount bankAccount, double amount) {
    bankAccount.setBalance(bankAccount.getBalance().add(BigDecimal.valueOf(amount)));
    update(bankAccount);

    return bankAccount;
  }

  /**
   * Retrieve active bank accounts from DB.
   *
   * @return List of accounts.
   */
  @Transactional
  public List<BankAccount> listAccounts() {
    return find("select ba from BankAccount", ImmutableMap.of(), BankAccount.class);
  }

  /**
   * Gets bank account by its number.
   *
   * @param number Unique account number.
   * @return Bank Account
   */
  @Transactional
  public BankAccount findAccount(String number) {
    return findSingleEntity("select ba from BankAccount ba where ba.number = :number",
        ImmutableMap.of("number", number), BankAccount.class);
  }

}
