package com.dmytrop.banking.resources;

import com.google.inject.Inject;

import com.dmytrop.banking.entities.Transaction;
import com.dmytrop.banking.services.TransactionService;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Rest API to access transactions data.
 */
@Path("transaction")
public class TransactionResource {

  private final TransactionService transactionService;

  @Inject
  public TransactionResource(TransactionService transactionService) {
    this.transactionService = transactionService;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response list() {
    List<Transaction> result = transactionService.findAll();
    return Response.ok(result).build();
  }
}
