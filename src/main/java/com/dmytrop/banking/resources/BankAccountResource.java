package com.dmytrop.banking.resources;

import com.dmytrop.banking.entities.BankAccount;
import com.dmytrop.banking.model.SendMoneyRequest;
import com.dmytrop.banking.model.ChargeAccountRequest;
import com.dmytrop.banking.services.BankAccountService;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * Rest Api resource to manipulate with bank accounts.
 */
@Path("bank-account")
public class BankAccountResource {

  private final BankAccountService bankAccountService;

  @Inject
  public BankAccountResource(BankAccountService bankAccountService) {
    this.bankAccountService = bankAccountService;
  }

  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  public Response sendMoney(SendMoneyRequest request) {
    try {
      boolean success = bankAccountService
          .sendMoney(request.getFromWallet(), request.getToWallet(), request.getAmount());
      if (!success) {
        return Response.notModified().build();
      }
    } catch (IllegalArgumentException ex) {
      return Response.status(Status.NOT_ACCEPTABLE).entity(ex.getMessage()).build();
    }
    return Response.ok().build();
  }

  /**
   * Creates new bank account with ZERO balance.
   *
   * @return Create account.
   */
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response createAccount() {
    BankAccount bankAccount = new BankAccount();
    bankAccount.setNumber(UUID.randomUUID().toString());
    bankAccount.setBalance(BigDecimal.valueOf(0, 2));

    BankAccount created = bankAccountService.create(bankAccount);
    return Response.ok(created).build();
  }

  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("balance")
  public Response chargeAccount(ChargeAccountRequest request) {
    BankAccount updated = bankAccountService.findAccount(request.getWallet());

    if (updated == null) {
      return Response.status(Status.NOT_FOUND).build();
    }

    updated = bankAccountService.chargeAccount(updated, request.getAmount());

    return Response.ok(updated).build();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response listAccounts() {
    List<BankAccount> result = bankAccountService.listAccounts();
    return Response.ok(result).build();
  }

  @GET
  @Path("find")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getAccount(@QueryParam("number") String number) {
    BankAccount account = bankAccountService.findAccount(number);
    if (account == null) {
      return Response.status(Status.NOT_FOUND).build();
    }
    return Response.ok(account).build();
  }
}
