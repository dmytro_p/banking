package com.dmytrop.banking.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.dmytrop.banking.entities.BankAccount;
import com.dmytrop.banking.entities.Transaction;
import com.dmytrop.banking.main.ServerRunner;
import com.dmytrop.banking.model.ChargeAccountRequest;
import com.dmytrop.banking.model.SendMoneyRequest;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

public class BankingResourceTest {

  private static ResteasyWebTarget target;
  private static ResteasyWebTarget txTarget;

  @BeforeClass
  public static void setUp() {
    final ServerRunner runner = new ServerRunner();
    runner.runForTest(8080);
    ResteasyClient client = new ResteasyClientBuilder().build();
    target = client.target(UriBuilder.fromPath("http://localhost:8080/rest/bank-account"));
    txTarget = client.target(UriBuilder.fromPath("http://localhost:8080/rest/transaction"));
  }

  @Test
  public void createAccount() {
    Response response = target.request()
        .post(Entity.entity(null, MediaType.APPLICATION_JSON), Response.class);
    BankAccount account = extractResponse(response, BankAccount.class);

    assertEquals(200, response.getStatus());
    assertEquals(BigDecimal.valueOf(0, 2), account.getBalance());
  }

  @Test
  public void chargeAccount() {
    Response response = target.request()
        .post(Entity.entity(null, MediaType.APPLICATION_JSON), Response.class);
    BankAccount account = extractResponse(response, BankAccount.class);

    assertEquals(BigDecimal.valueOf(0, 2), account.getBalance());

    ChargeAccountRequest chargeRequest = new ChargeAccountRequest(account.getNumber(), 110.12);
    response = target.path("balance").request()
        .put(Entity.entity(chargeRequest, MediaType.APPLICATION_JSON), Response.class);
    account = extractResponse(response, BankAccount.class);

    assertEquals(200, response.getStatus());
    assertEquals(BigDecimal.valueOf(110.12), account.getBalance());
  }

  @Test
  public void chargeAccount_incorrectAccountNumber() {
    ChargeAccountRequest chargeRequest = new ChargeAccountRequest("some_fake_number", 110.12);

    Response response = target.path("balance").request()
        .put(Entity.entity(chargeRequest, MediaType.APPLICATION_JSON), Response.class);
    response.close();

    assertEquals(404, response.getStatus());
  }

  @Test
  public void sendMoney() {
    Response response = target.request()
        .post(Entity.entity(null, MediaType.APPLICATION_JSON), Response.class);
    BankAccount account1 = extractResponse(response, BankAccount.class);

    response = target.request()
        .post(Entity.entity(null, MediaType.APPLICATION_JSON), Response.class);
    BankAccount account2 = extractResponse(response, BankAccount.class);

    ChargeAccountRequest chargeRequest = new ChargeAccountRequest(account1.getNumber(), 20.00);
    response = target.path("balance").request()
        .put(Entity.entity(chargeRequest, MediaType.APPLICATION_JSON), Response.class);
    account1 = extractResponse(response, BankAccount.class);

    SendMoneyRequest request = new SendMoneyRequest(account1.getNumber(),
        account2.getNumber(), 10.00);
    response = target.request()
        .put(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE),
            Response.class);
    assertEquals(200, response.getStatus());
    response.close();

    response = target.path("find").queryParam("number", account2.getNumber())
        .request().get(Response.class);
    account2 = extractResponse(response, BankAccount.class);

    assertEquals(BigDecimal.valueOf(10.00).stripTrailingZeros(),
        account2.getBalance().stripTrailingZeros());

    final BankAccount from = account1;
    final BankAccount to = account2;

    response = txTarget.request().get(Response.class);
    List<Transaction> txs = response.readEntity(
        new GenericType<List<Transaction>>(new ParameterizedType() {
          public Type[] getActualTypeArguments() {
            return new Type[]{Transaction.class};
          }

          public Type getRawType() {
            return ArrayList.class;
          }

          public Type getOwnerType() {
            return ArrayList.class;
          }
        }));
    response.close();

    Optional<Transaction> optional = txs.stream().filter(
        tx -> tx.getCredit().getNumber().equals(to.getNumber()) &&
            tx.getDebit().getNumber().equals(from.getNumber())).findFirst();
    assertTrue(optional.isPresent());
    assertEquals(BigDecimal.valueOf(1000, 2), optional.get().getAmount());
  }

  @Test
  public void sendMoney_insufficientBalance() {
    Response response = target.request()
        .post(Entity.entity(null, MediaType.APPLICATION_JSON), Response.class);
    BankAccount account1 = extractResponse(response, BankAccount.class);

    response = target.request()
        .post(Entity.entity(null, MediaType.APPLICATION_JSON), Response.class);
    BankAccount account2 = extractResponse(response, BankAccount.class);

    ChargeAccountRequest chargeRequest = new ChargeAccountRequest(account1.getNumber(), 15.00);
    response = target.path("balance").request()
        .put(Entity.entity(chargeRequest, MediaType.APPLICATION_JSON), Response.class);
    account1 = extractResponse(response, BankAccount.class);

    SendMoneyRequest request = new SendMoneyRequest(account1.getNumber(),
        account2.getNumber(), 30.00);
    response = target.request()
        .put(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE),
            Response.class);
    assertEquals(304, response.getStatus());
    response.close();
  }

  private <T> T extractResponse(Response response, Class<T> cl) {
    try {
      return response.readEntity(cl);
    } finally {
      response.close();
    }
  }
}
